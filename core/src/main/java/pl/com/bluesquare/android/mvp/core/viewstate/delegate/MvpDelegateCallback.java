package pl.com.bluesquare.android.mvp.core.viewstate.delegate;

import pl.com.bluesquare.android.mvp.core.presenter.MvpPresenter;
import pl.com.bluesquare.android.mvp.core.view.MvpView;

/**
 * Created by work on 05.07.15.
 */
public interface MvpDelegateCallback<V extends MvpView, P extends MvpPresenter<V>> {
}
