package pl.com.bluesquare.android.mvp.core.viewgroup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.ButterKnife;

/**
 * Created by Luk.
 */
public class BaseViewGroup extends LinearLayout {

    protected View view;


    public BaseViewGroup(Context context) {
        super(context);
        onInflateView();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ButterKnife.bind(this, view);
        onCreated();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ButterKnife.unbind(this);
        onDestroy();
    }

    private void onInflateView() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        view = inflater.inflate(getLayoutRes(), this, true);

    }

    protected void onCreated(){}
    protected void onDestroy(){}

    protected int getLayoutRes() {
        return 0;
    }
}
