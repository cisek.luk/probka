package pl.com.bluesquare.android.mvp.core.view;

/**
 * Created by work on 05.07.15.
 */
public interface MvpLceView<M> extends MvpView {

    public void showLoading(boolean swipeToRefresh);

    public void showContent();

    public void showError(Throwable th, boolean swipeToRefresh);

    public void setData(M data);

    public void loadData(boolean swipeToRefresh);

}
