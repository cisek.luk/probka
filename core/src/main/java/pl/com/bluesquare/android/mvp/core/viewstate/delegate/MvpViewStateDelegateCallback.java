package pl.com.bluesquare.android.mvp.core.viewstate.delegate;

import pl.com.bluesquare.android.mvp.core.presenter.MvpPresenter;
import pl.com.bluesquare.android.mvp.core.view.MvpView;
import pl.com.bluesquare.android.mvp.core.viewstate.ViewState;

/**
 * Created by work on 05.07.15.
 */
public interface MvpViewStateDelegateCallback<V extends MvpView, P extends MvpPresenter<V>>
        extends MvpDelegateCallback<V, P> {


    public ViewState<V> getViewState();

    public void setViewState(ViewState<V> viewState);

    public ViewState<V> createViewState();

    public void setRestoringViewState(boolean restoringViewState);

    public boolean isRestoringViewState();

    public void onViewStateInstanceRestored(boolean instanceStateRetained);

    public void onNewViewStateInstance();
}
