package pl.com.bluesquare.android.main.drawermenu;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import butterknife.Bind;
import pl.com.bluesquare.android.main.R;
import pl.com.bluesquare.android.mvp.core.viewgroup.MvpViewGroup;

/**
 * Created by Luk.
 */
public class DrawerMenuViewGroup extends MvpViewGroup<DrawerMenuView, DrawerMenuPresenter>
        implements DrawerMenuView {

    @Bind(R.id.drawer_menu_lv)
    ListView lvMenu;

    public DrawerMenuViewGroup(Context context) {
        super(context);
    }

    @Override
    protected DrawerMenuPresenter createPresenter() {
        return new DrawerMenuPresenter();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.drawer_menu;
    }

    @Override
    protected void onCreated() {
        super.onCreated();
        initComponents();
    }


    private void initComponents() {
        String[] items = { "Milk", "Butter", "Yogurt", "Toothpaste", "Ice Cream" };

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, items);

        lvMenu.setAdapter(adapter);
    }
}
