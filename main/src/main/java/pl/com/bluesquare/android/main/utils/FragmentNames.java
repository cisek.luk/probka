package pl.com.bluesquare.android.main.utils;

/**
 * Created by Luk.
 */
public class FragmentNames {


    public static final String HOME_FRAGMENT_TAG = "home_fragment_tag";


    public static final String ADD_FRAGMENT_TAG = "add_fragment_tag";


    public static final String EDIT_FRAGMENT_TAG = "edit_fragment_tag";



}
