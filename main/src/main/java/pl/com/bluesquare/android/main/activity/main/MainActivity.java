package pl.com.bluesquare.android.main.activity.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import butterknife.Bind;
import pl.com.bluesquare.android.login.activity.init.InitActivity;
import pl.com.bluesquare.android.main.R;
import pl.com.bluesquare.android.main.draweraccount.DrawerAccountViewGroup;
import pl.com.bluesquare.android.mvp.core.activity.MvpActivity;

/**
 * Created by Luk.
 */

public class MainActivity extends MvpActivity<MainView, MainPresenter>
        implements MainView, NavigationView.OnNavigationItemSelectedListener {

    public static final int LOGIN_CODE = 1111;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;
    @Bind(R.id.navigation_view)
    NavigationView navigationView;

    private int currentPosition;

    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);

//        initNavigationDrawer();
//        navigationView.addHeaderView(new DrawerAccountViewGroup(this));
//        navigationView.setNavigationItemSelectedListener(this);

        Intent intent = new Intent(this, InitActivity.class);
        startActivityForResult(intent, LOGIN_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LOGIN_CODE && resultCode == RESULT_OK) {
            //update main fragment

        } else if(requestCode == LOGIN_CODE && resultCode == RESULT_CANCELED) {
            finish();
        }
    }

    @Override
    public void switchFragment(String fragmentName) {
        getFragmentManager().beginTransaction()
//                .setCustomAnimations(pl.com.bluesquare.android.login.R.anim.slide, pl.com.bluesquare.android.login.R.anim.slide_in_right)
                .replace(R.id.container_main, presenter.getFragment(getFragmentManager(), fragmentName))
                .commit();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);

        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                currentPosition = 0;
                presenter.startHome();
                return true;

            case R.id.navigation_add:
                currentPosition = 1;
                presenter.startAdd();
                return true;

            case R.id.navigation_edit:
                currentPosition = 2;
                presenter.startEdit();
                return true;

            case R.id.navigation_logout:
                currentPosition = 3;
                presenter.startLogout();
                return true;

            default:
                break;
        }
        return false;

    }

    private void initNavigationDrawer() {
        if (toolbar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationIcon(android.R.drawable.ic_delete);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.openDrawer(GravityCompat.START);
                }
            });
        }

    }



}
