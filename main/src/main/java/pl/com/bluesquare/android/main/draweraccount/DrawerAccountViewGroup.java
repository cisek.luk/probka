package pl.com.bluesquare.android.main.draweraccount;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.OnClick;
import pl.com.bluesquare.android.main.R;
import pl.com.bluesquare.android.mvp.core.viewgroup.MvpViewGroup;

/**
 * Created by Luk.
 */
public class DrawerAccountViewGroup extends MvpViewGroup<DrawerAccountView, DrawerAccountPresenter>
        implements DrawerAccountView {

    @Bind(R.id.drawer_header_iv_avatar)
    ImageView ivAvatar;
    @Bind(R.id.drawer_header_txt_username)
    TextView txtName;
    @Bind(R.id.drawer_header_txt_email)
    TextView txtEmail;

    public DrawerAccountViewGroup(Context context) {
        super(context);
    }

    @Override
    protected DrawerAccountPresenter createPresenter() {
        return new DrawerAccountPresenter();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.drawer_header;
    }


    @Override
    protected void onCreated() {
        super.onCreated();
        initComponents();
    }

    @OnClick(R.id.drawer_header_iv_avatar)
    public void avatarClick() {
        Log.d("TAG", "avatar click");
    }

    private void initComponents() {
        txtName.setText("TEst name");
    }

}
