package pl.com.bluesquare.android.main.activity.main;

import android.app.Fragment;
import android.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import pl.com.bluesquare.android.main.utils.FragmentNames;
import pl.com.bluesquare.android.main.utils.event.FragmentSwitchEvent;
import pl.com.bluesquare.android.mvp.core.presenter.BasePresenter;

/**
 * Created by Luk.
 */
public class MainPresenter extends BasePresenter<MainView> {

    public Fragment getFragment(FragmentManager fragmentManager, String fragmentName) {
        if(TextUtils.isEmpty(fragmentName)) {
            throw new IllegalArgumentException("Fragment name is empty or null!");
        }

        if (fragmentManager.findFragmentByTag(fragmentName) != null ) {
            Log.d(TAG, "fragment found");
            return fragmentManager.findFragmentByTag(fragmentName);
        }

//        if(FragmentNames.HOME_FRAGMENT_TAG.equals(fragmentName)) {
//            Log.d(TAG, "return fragment home");
//            return new HomeFragment();
//        }
//
//        if(FragmentNames.ADD_FRAGMENT_TAG.equals(fragmentName)) {
//            Log.d(TAG, "return fragment ADD");
//            return new AddFragment();
//        }
//
//        if(FragmentNames.EDIT_FRAGMENT_TAG.equals(fragmentName)) {
//            Log.d(TAG, "return fragment EDIT");
//            return new EditFragment();
//        }

        return null;
    }

    public void startHome() {
        getView().switchFragment(FragmentNames.HOME_FRAGMENT_TAG);
    }

    public void startAdd() {
        getView().switchFragment(FragmentNames.ADD_FRAGMENT_TAG);
    }

    public void startEdit() {
        getView().switchFragment(FragmentNames.EDIT_FRAGMENT_TAG);
    }

    public void startLogout() {

    }


    @Override
    public void attachView(MainView view) {
        super.attachView(view);
        EventBus.getDefault().register(this);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(FragmentSwitchEvent ev) {
        Log.d(TAG, "received " + ev.fragmentName);
        if(isViewAttached()) {
            getView().switchFragment(ev.fragmentName);
        }
    }




}
