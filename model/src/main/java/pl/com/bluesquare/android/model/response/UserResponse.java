package pl.com.bluesquare.android.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Luk on 08.10.15.
 */
public class UserResponse {

    @SerializedName("objectId")
    public String objectId;

    @SerializedName("username")
    public String username;

    @SerializedName("firstName")
    public String firstName;

    @SerializedName("lastName")
    public String lastName;

    @SerializedName("nick")
    public String nick;

    @Override
    public String toString() {
        return "UserResponse{" +
                "objectId='" + objectId + '\'' +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", nick='" + nick + '\'' +
                '}';
    }
}
