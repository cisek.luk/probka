package pl.com.bluesquare.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Luk on 08.10.15.
 */
public class ResetPassword {

    @SerializedName("email")
    public String email;

    public ResetPassword(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ResetPassword{" +
                "email='" + email + '\'' +
                '}';
    }
}
