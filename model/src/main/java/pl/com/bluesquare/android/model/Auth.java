package pl.com.bluesquare.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Luk.
 */
public class Auth {

    @SerializedName("username")
    public String username;

    @SerializedName("password")
    public String password;

    @SerializedName("email")
    public String email;


    public Auth(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Auth{" +
                ", login='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
