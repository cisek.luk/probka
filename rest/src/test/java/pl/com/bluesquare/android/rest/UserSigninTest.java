package pl.com.bluesquare.android.rest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import pl.com.bluesquare.android.model.response.SigninResponse;
import pl.com.bluesquare.android.rest.clients.UserRestClient;
import retrofit.Callback;
import retrofit.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Luk on 08.10.15.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserSigninTest {

    public UserRestClient restClient;

    @Before
    public void setUp() {
        restClient = new UserRestClient();
    }

    @After
    public void tearDown() {
        restClient = null;
    }

    @Test
    public void testSigninSuccess() throws InterruptedException, IOException {
        Map<String, String> map = new HashMap<>();
        map.put("username", "cisek.luk@gmail.com");
        map.put("password", "1234");

        final AtomicReference<Response<SigninResponse>> responseRef = new AtomicReference<>();
        final CountDownLatch latch = new CountDownLatch(1);

        restClient.getSignin(map, new Callback<SigninResponse>() {

            @Override
            public void onResponse(Response<SigninResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }


            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<SigninResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isTrue();
        assertThat(res.code()).isEqualTo(200);
        assertThat(res.body().objectId).isNotEmpty();
        assertThat(res.body().token).isNotEmpty();
    }


    @Test
    public void testSigninFailed() throws InterruptedException, IOException {
        Map<String, String> map = new HashMap<>();
        map.put("username", "cisek.luk@gmail.com");
        map.put("password", "123");

        final AtomicReference<Response<SigninResponse>> responseRef = new AtomicReference<>();
        final CountDownLatch latch = new CountDownLatch(1);

        restClient.getSignin(map, new Callback<SigninResponse>() {

            @Override
            public void onResponse(Response<SigninResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }


            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<SigninResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isFalse();
        assertThat(res.code()).isEqualTo(404);
        assertThat(res.body()).isNull();
    }

    @Test
    public void testSigninUserNotRegisterFailed() throws InterruptedException, IOException {
        Map<String, String> map = new HashMap<>();
        map.put("username", "cisek@gmail.com");
        map.put("password", "12321");

        final AtomicReference<Response<SigninResponse>> responseRef = new AtomicReference<>();
        final CountDownLatch latch = new CountDownLatch(1);

        restClient.getSignin(map, new Callback<SigninResponse>() {

            @Override
            public void onResponse(Response<SigninResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }


            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<SigninResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isFalse();
        assertThat(res.code()).isEqualTo(404);
        assertThat(res.body()).isNull();
    }

}
