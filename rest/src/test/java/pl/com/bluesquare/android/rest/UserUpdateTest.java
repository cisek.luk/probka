package pl.com.bluesquare.android.rest;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import pl.com.bluesquare.android.model.UpdateUser;
import pl.com.bluesquare.android.model.response.SigninResponse;
import pl.com.bluesquare.android.model.response.UpdateUserResponse;
import pl.com.bluesquare.android.rest.clients.UserRestClient;
import retrofit.Callback;
import retrofit.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Luk on 08.10.15.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserUpdateTest {

    public UserRestClient restClient;
    public AtomicReference<Response<UpdateUserResponse>> responseRef;
    public CountDownLatch latch;

    private static String token;

    @BeforeClass
    public static void init() throws InterruptedException, IOException {
        signup();
    }

    @Before
    public void setUp() {
        restClient = new UserRestClient(token);
        responseRef = new AtomicReference<>();
        latch = new CountDownLatch(1);
    }

    @After
    public void tearDown() {
        restClient = null;
        responseRef = null;
        latch = null;
    }

    @Test
    public void testUpdateSuccess() throws Exception {
        String updateId = "5fMHUj5adE";
        Random rand = new Random();
        UpdateUser update = new UpdateUser("nick" + rand.nextInt(), "first" + rand.nextInt(), "last" + rand.nextInt());

        restClient.getUpdateUser(updateId, update, new Callback<UpdateUserResponse>() {
            @Override
            public void onResponse(Response<UpdateUserResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<UpdateUserResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isTrue();
        assertThat(res.code()).isEqualTo(200);
    }

    @Test
    public void testUpdateFailed() throws Exception {
        String updateId = "";
        Random rand = new Random();
        UpdateUser update = new UpdateUser("nick" + rand.nextInt(), "first" + rand.nextInt(), "last" + rand.nextInt());

        restClient.getUpdateUser(updateId, update, new Callback<UpdateUserResponse>() {
            @Override
            public void onResponse(Response<UpdateUserResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<UpdateUserResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isFalse();
        assertThat(res.code()).isEqualTo(404);
    }


    private static void signup() throws InterruptedException, IOException {
        Map<String, String> map = new HashMap<>();
        map.put("username", "cisek.luk@gmail.com");
        map.put("password", "1234");
        UserRestClient restClient = new UserRestClient();
        final AtomicReference<Response<SigninResponse>> responseRef = new AtomicReference<>();
        final CountDownLatch latch = new CountDownLatch(1);

        restClient.getSignin(map, new Callback<SigninResponse>() {

            @Override
            public void onResponse(Response<SigninResponse> response) {
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);
        token = responseRef.get().body().token;
    }
}
