package pl.com.bluesquare.android.rest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import pl.com.bluesquare.android.model.response.UserResponse;
import pl.com.bluesquare.android.rest.clients.UserRestClient;
import retrofit.Callback;
import retrofit.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Luk on 08.10.15.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserDataTest {

    public UserRestClient restClient;
    public AtomicReference<Response<UserResponse>> responseRef;
    public CountDownLatch latch;

    @Before
    public void setUp() {
        restClient = new UserRestClient();
        responseRef = new AtomicReference<>();
        latch = new CountDownLatch(1);
    }

    @After
    public void tearDown() {
        restClient = null;
        responseRef = null;
        latch = null;
    }

    @Test
    public void testGetUserSuccess() throws Exception {
        String userId = "5fMHUj5adE";

        restClient.getUser(userId, new Callback<UserResponse>() {
            @Override
            public void onResponse(Response<UserResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<UserResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isTrue();
        assertThat(res.code()).isEqualTo(200);
        assertThat(res.body().objectId).isNotEmpty();
        assertThat(res.body().username).isNotEmpty();
    }

    @Test
    public void testGetUserFailed() throws Exception {
        String userId = "0000001";

        restClient.getUser(userId, new Callback<UserResponse>() {
            @Override
            public void onResponse(Response<UserResponse> response) {
                System.out.println("Show code :: " + response.code());
                System.out.println("Show is success :: " + response.isSuccess());
                System.out.println("Show is msg :: " + response.message());
                if (response.code() == 200 || response.code() == 201) {
                    System.out.println("Show :: " + response.body().toString());
                }
                responseRef.set(response);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
        latch.await(5, TimeUnit.SECONDS);

        Response<UserResponse> res = responseRef.get();
        assertThat(res).isNotNull();
        assertThat(res.isSuccess()).isFalse();
        assertThat(res.code()).isEqualTo(404);
        assertThat(res.body()).isNull();
    }


}
