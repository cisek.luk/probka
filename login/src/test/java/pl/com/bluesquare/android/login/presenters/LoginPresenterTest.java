package pl.com.bluesquare.android.login.presenters;

import android.app.Activity;

import com.google.common.eventbus.EventBus;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;

import pl.com.bluesquare.android.login.login.LoginPresenter;
import pl.com.bluesquare.android.login.login.LoginView;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.model.response.SigninResponse;
import retrofit.Callback;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by Luk on 08.10.15.
 */

// Mockito runner require stub TextUtils
//@RunWith(MockitoJUnitRunner.class)
@RunWith(RobolectricTestRunner.class)
public class LoginPresenterTest {


    private Callback<SigninResponse> callback;
    private LoginView loginView;
    private LoginPresenter presenter;


    @Before
    public void setUp() {
        loginView = Mockito.mock(LoginView.class);
        callback = Mockito.mock(Callback.class);

        presenter = new LoginPresenter();
        presenter.attachView(loginView);
    }

    @After
    public void tearDown() {
        presenter = null;
        loginView = null;
        callback = null;
    }

    @Test
    public void testExecute() throws Exception {
        presenter.execute("test@t.pl", "1234");

        Mockito.verify(loginView).showLoading();
    }

    @Test
    public void testExecuteErrorEmailFromat() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("testt.pl", "1234");

        Mockito.verify(loginView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmailFormatException.class);
    }

    @Test
    public void testExecuteErrorsEmailEmpty() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("", "1234");

        Mockito.verify(loginView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("0");
    }

    @Test
    public void testExecuteErrorsPasswordEmpty() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("test@t.pl", "");

        Mockito.verify(loginView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("1");
    }


//    @Test
//    public void testResponse() throws Exception {
//        final Response<SigninResponse> response = Response.success(null);
//        doAnswer(new Answer() {
//            @Override
//            public Object answer(InvocationOnMock invocation) throws Throwable {
//                ((LoginPresenter)invocation.getArguments()[0]).onResponse(response);
//                return null;
//            }
//        }).when(callback).onResponse(any(Callback.class));
//
//
//        ArgumentCaptor<Response> argument = ArgumentCaptor.forClass(Response.class);
//        presenter.execute("test@t.pl", "1234");
//
//
//
//        Mockito.verify(loginView).showLoading();
//        Mockito.verify(callback, timeout(10000)).onResponse(argument.capture());
//
//        assertThat(argument.capture().body()).isEqualTo(SigninResponse.class);
//
//        Mockito.verify(loginView, timeout(15000)).showSuccess();
//
//    }

}
