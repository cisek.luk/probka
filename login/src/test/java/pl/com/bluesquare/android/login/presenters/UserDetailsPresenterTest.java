package pl.com.bluesquare.android.login.presenters;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;

import pl.com.bluesquare.android.login.userdetails.UserDetailsPresenter;
import pl.com.bluesquare.android.login.userdetails.UserDetailsView;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.model.response.SignupResponse;
import retrofit.Callback;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Luk on 08.10.15.
 */
@RunWith(RobolectricTestRunner.class)
public class UserDetailsPresenterTest {

    private Callback<SignupResponse> callback;
    private UserDetailsView registrationView;
    private UserDetailsPresenter presenter;


    @Before
    public void setUp() {
        registrationView = Mockito.mock(UserDetailsView.class);
        callback = Mockito.mock(Callback.class);

        presenter = new UserDetailsPresenter();
        presenter.attachView(registrationView);
    }

    @After
    public void tearDown() {
        presenter = null;
        registrationView = null;
        callback = null;
    }

    @Test
    public void testExecute() throws Exception {
        presenter.execute("1234", "test", "1234", "wsxc");

        Mockito.verify(registrationView).showLoading();
    }

    @Test
    public void testExecuteErrorNickEmpty() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("1234", "", "1234", "wsxc");

        Mockito.verify(registrationView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmptyFieldException.class);
    }

    @Test
    public void testExecuteErrorFirstNameEmpty() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("1234", "test", "", "wsxc");

        Mockito.verify(registrationView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("0");
    }

    @Test
    public void testExecuteErrorLastNameEmpty() throws Exception {
        ArgumentCaptor<Exception> argument = ArgumentCaptor.forClass(Exception.class);
        presenter.execute("1234", "test", "1234", "");

        Mockito.verify(registrationView).showError(argument.capture());
        assertThat(argument.getValue()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("1");
    }


}
