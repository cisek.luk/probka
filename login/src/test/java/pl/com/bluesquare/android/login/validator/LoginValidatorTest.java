package pl.com.bluesquare.android.login.validator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import pl.com.bluesquare.android.login.BuildConfig;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.login.utils.validator.LoginValidator;
import pl.com.bluesquare.android.login.utils.validator.RegistrationValidator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Luk on 07.10.15.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class LoginValidatorTest {

    private LoginValidator validator;

    @Before

    public void setUp() {
        validator = new LoginValidator();
    }

    @After
    public void tearDown() {
        validator = null;
    }

    @Test
    public void testValidData() {
        String email = "test@tt.com";
        String pswd = "paswd";

        assertThat(validator.isValid(email, pswd)).isTrue();
    }

    @Test
    public void testEmptyEmailException() {
        String email = "";
        String pswd = "paswd";
        String confpswd = "paswd";

        assertThat(validator.isValid(email, pswd, confpswd)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class);
    }

    @Test
    public void testEmailFormatException() {
        String email = "test.tt.com";
        String pswd = "paswd";
        String confpswd = "paswd";

        assertThat(validator.isValid(email, pswd, confpswd)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmailFormatException.class);
    }

    @Test
    public void testEmailEmptyException() {
        String email = "";
        String pswd = "123";

        assertThat(validator.isValid(email, pswd)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("0");
    }

    @Test
    public void testPasswordEmptyException() {
        String email = "test@tt.com";
        String pswd = "";

        assertThat(validator.isValid(email, pswd)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("1");
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testMissingField() {
        String email = "test@tt.com";

        assertThat(validator.isValid(email)).isFalse();
    }

}
