package pl.com.bluesquare.android.login.validator;

import android.text.TextUtils;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import pl.com.bluesquare.android.login.BuildConfig;
import pl.com.bluesquare.android.login.utils.exceptions.EmailEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordNotMatchException;
import pl.com.bluesquare.android.login.utils.validator.RegistrationValidator;

/**
 * Created by Luk on 07.10.15.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class RegistrationValidatorTest {

    private RegistrationValidator validator;

    @Before
    public void setUp() {
        validator = new RegistrationValidator();
    }

    @After
    public void tearDown() {
        validator = null;
    }

    @Test
    public void testValidData() {
        String email = "test@tt.com";
        String pswd = "paswd";
        String confpswd = "paswd";

        assertThat(validator.isValid(email, pswd, confpswd)).isTrue();
    }

    @Test
    public void testEmptyEmailException() {
        String email = "";
        String pswd = "paswd";
        String confpswd = "paswd";

        assertThat(validator.isValid(email, pswd, confpswd)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class);
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("0");
    }

    @Test
    public void testEmptyPasswordException() {
        String email = "test@tt.com";
        String pswd = "";
        String confpswd = "paswd";

        assertThat(validator.isValid(email, pswd, confpswd)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class);
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("1");
    }

    @Test
    public void testEmptyConfirmPasswordException() {
        String email = "test@tt.com";
        String pswd = "paswd";
        String confpswd = "";

        assertThat(validator.isValid(email, pswd, confpswd)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class);
        assertThat(validator.getError()).isExactlyInstanceOf(EmptyFieldException.class).hasMessage("2");
    }

    @Test
    public void testToManyArgsException() {
        String email = "test@tt.com";
        String pswd = "paswd";
        String confpswd = "paswd";
        String confpswd2 = "paswd";

        assertThat(validator.isValid(email, pswd, confpswd, confpswd2)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(IllegalArgumentException.class);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testToSmallArgsException() {
        String email = "test@tt.com";
        String pswd = "paswd";


        assertThat(validator.isValid(email, pswd)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void testEmailFormatException() {
        String email = "test.tt.com";
        String pswd = "paswd";
        String confpswd = "paswd";

        assertThat(validator.isValid(email, pswd, confpswd)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(EmailFormatException.class);
    }

    @Test
    public void testPasswordNotMatchException() {
        String email = "test@tt.com";
        String pswd = "123";
        String confpswd = "paswd";

        assertThat(validator.isValid(email, pswd, confpswd)).isFalse();
        assertThat(validator.getError()).isExactlyInstanceOf(PasswordNotMatchException.class);
    }

}
