package pl.com.bluesquare.android.login.activity.init;

import pl.com.bluesquare.android.mvp.core.view.MvpView;

/**
 * Created by work on 05.07.15.
 */
public interface InitView extends MvpView {


    public void switchFragment(String fragmentName);

    public void showSnackbar(boolean isShow);

    public void showSnackbarNoInternet(boolean isShow);

    public void showSnackbarUserRegister();

    public void showSnackbarUserNotRegister();

    public void finishActivity();



}
