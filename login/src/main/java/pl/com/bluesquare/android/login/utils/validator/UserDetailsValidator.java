package pl.com.bluesquare.android.login.utils.validator;

import android.text.TextUtils;

import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;

/**
 * Created by Luk on 08.10.15.
 */
public class UserDetailsValidator extends BaseValidator {

    @Override
    public boolean isValid(String... values) {
        if (TextUtils.isEmpty(values[0])) {
            error = new EmptyFieldException("0");
            return false;

        } else if (TextUtils.isEmpty(values[1])) {
            error = new EmptyFieldException("1");
            return false;

        } else if (TextUtils.isEmpty(values[2])) {
            error = new EmptyFieldException("2");
            return false;

        } else if (values.length != 3) {
            error = new IllegalArgumentException("To many parameters!");
            return false;

        } else {
            return true;
        }
    }

}
