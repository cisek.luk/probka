package pl.com.bluesquare.android.login.utils.exceptions;

/**
 * Created by Luk.
 */
public class PasswordException extends Exception {

    public PasswordException(String detailMessage) {
        super(detailMessage);
    }
}
