package pl.com.bluesquare.android.login.restorepassword;

import android.util.Log;

import java.util.Objects;

import de.greenrobot.event.EventBus;
import pl.com.bluesquare.android.login.utils.FragmentName;
import pl.com.bluesquare.android.login.utils.events.ActionEvent;
import pl.com.bluesquare.android.login.utils.events.FragmentSwitchEvent;
import pl.com.bluesquare.android.login.utils.exceptions.EmailEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.UserNotRegisterException;
import pl.com.bluesquare.android.login.utils.validator.BaseValidator;
import pl.com.bluesquare.android.login.utils.validator.RegistrationValidator;
import pl.com.bluesquare.android.login.utils.validator.ResetPasswordValidator;
import pl.com.bluesquare.android.model.ResetPassword;
import pl.com.bluesquare.android.model.User;
import pl.com.bluesquare.android.model.response.ResetPasswordResponse;
import pl.com.bluesquare.android.mvp.retrofit.presenter.RetrofitPresenter;
import pl.com.bluesquare.android.rest.clients.UserRestClient;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by work on 05.07.15.
 */
public class PasswordRestorePresenter extends RetrofitPresenter<PasswordRestoreView, ResetPasswordResponse> {

    private UserRestClient rest;
    private BaseValidator validator;

    public PasswordRestorePresenter() {
        validator = new ResetPasswordValidator();
        rest = new UserRestClient();
    }

    public void execute(String email) {
        if (isViewAttached()) {
            if (validator.isValid(email)) {
                getView().showLoading();
                rest.getResetPassword(new ResetPassword(email), this);

            } else {
                getView().showError(validator.getError());
            }
        }
    }

    @Override
    public void onResponse(Response<ResetPasswordResponse> response) {
        if(isViewAttached() && response.code() == 200) {
            getView().showSuccess();

        } else if (isViewAttached()) {
            getView().showError(new UserNotRegisterException());

        } else {
        }
    }

    public void back() {
        if(isViewAttached()) {
            EventBus.getDefault().post(new FragmentSwitchEvent(FragmentName.LOGIN_FRAGMENT_TAG));
        }
    }

    public void noInternet() {
        if(isViewAttached()) {
            EventBus.getDefault().post(new ActionEvent(ActionEvent.ACTION_SHOW_SNACKBAR_NOINTERNET));
        }
    }

    public void userNotRegister() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new ActionEvent(ActionEvent.ACTION_SHOW_SNACKBAR_USER_NOT_REGISTER));
        }
    }


}
