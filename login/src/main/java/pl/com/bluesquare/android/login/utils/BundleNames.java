package pl.com.bluesquare.android.login.utils;

/**
 * Created by Luk on 08.10.15.
 */
public final class BundleNames {

    public static final String LOGIN_BUNDLE_KEY = "login_bundle_key";

    public static final String LOGIN_BUNDLE_EMAIL = "login_bundle_email";

    public static final String LOGIN_BUNDLE_PASSWORD = "login_bundle_password";

    public static final String LOGIN_BUNDLE_REPEAT = "login_bundle_repeat";
//          ----------------->>>>>>>>>>>>>>>>>>>>>


    public static final String REGISTRATION_BUNDLE_KEY = "registration_bundle_key";

    public static final String REGISTRATION_BUNDLE_EMAIL = "registration_bundle_email";

    public static final String REGISTRATION_BUNDLE_PASSWORD = "registration_bundle_password";

    public static final String REGISTRATION_BUNDLE_CONFIRM_PASSWROD = "registration_bundle_confirm_password";
//          ----------------->>>>>>>>>>>>>>>>>>>>>


    public static final String USER_DETAILS_BUNDLE_KEY = "user_details_bundle_key";

    public static final String USER_DETAILS_BUNDLE_NICK = "user_details_bundle_nick";

    public static final String USER_DETAILS_BUNDLE_FIRST_NAME = "user_details_bundle_first_name";

    public static final String USER_DETAILS_BUNDLE_LAST_NAME = "user_details_bundle_last_name";
//          ----------------->>>>>>>>>>>>>>>>>>>>>


    public static final String RESET_PSWD_BUNDLE_KEY = "reset_pswd_bundle_key";

    public static final String RESET_PSWD_BUNDLE_NICK = "reset_pswd_bundle_email";


}
