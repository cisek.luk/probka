package pl.com.bluesquare.android.login.login;


import pl.com.bluesquare.android.model.User;
import pl.com.bluesquare.android.model.response.SigninResponse;
import pl.com.bluesquare.android.model.response.UserResponse;
import pl.com.bluesquare.android.mvp.retrofit.view.MvpRetrofitView;

/**
 * Created by work on 05.07.15.
 */
public interface LoginView extends MvpRetrofitView {

    public void saveCreadential(SigninResponse user);
}
