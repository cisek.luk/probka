package pl.com.bluesquare.android.login.utils.validator;

import android.text.TextUtils;

import java.util.regex.Pattern;

import pl.com.bluesquare.android.login.utils.exceptions.EmailEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordNotMatchException;

/**
 * Created by Luk.
 */
public abstract class BaseValidator implements Validator {

    protected static final Pattern VALID_EMAIL_ADDRESS =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    protected Throwable error;

    @Override
    public Throwable getError() {
        return error;
    }

    protected boolean isEmailValid(final String email) {
        if (!VALID_EMAIL_ADDRESS.matcher(email).matches()) {
            error = new EmailFormatException();
            return false;

        } else {
            return true;
        }
    }

    protected boolean isPasswordValid(final String pswd) {
        return isPasswordValid(pswd, pswd);
    }

    protected boolean isPasswordValid(final String pswd, final String confPaswd) {
        if (!pswd.equals(confPaswd)) {
            error = new PasswordNotMatchException();
            return false;

        } else {
            return true;
        }
    }


}
