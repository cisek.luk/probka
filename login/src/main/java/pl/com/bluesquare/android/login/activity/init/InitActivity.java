package pl.com.bluesquare.android.login.activity.init;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;

import pl.com.bluesquare.android.login.R;
import pl.com.bluesquare.android.login.utils.FragmentName;
import pl.com.bluesquare.android.login.utils.wrapper.SnackbarWrapper;
import pl.com.bluesquare.android.mvp.core.activity.MvpActivity;

public class InitActivity extends MvpActivity<InitView, InitPresenter> implements InitView {

    private CoordinatorLayout coordinatorLayout;
    private String fragmentTag;
    private SnackbarWrapper snackbarWrapper;

    private int count;

    @Override
    protected InitPresenter createPresenter() {
        return new InitPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initComponents();

        if (savedInstanceState == null) {
            switchFragment(FragmentName.LOGIN_FRAGMENT_TAG);

        } else {
            switchFragment(savedInstanceState.getString(FragmentName.FRAGMENT_KEY));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(FragmentName.FRAGMENT_KEY, fragmentTag);
    }

    @Override
    public void switchFragment(String fragmentName) {
        fragmentTag = fragmentName;
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_in_left, R.animator.slide_down)
                .replace(R.id.fragmentContainer, presenter.getFragment(getFragmentManager(), fragmentName), fragmentName)
                .commit();
    }

    @Override
    public void onBackPressed() {
        count++;
        if(count == 2) {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.string_dialog_exit_title))
                    .setMessage(getString(R.string.string_dialog_exit_title))
                    .setPositiveButton(getString(R.string.string_dialog_btn_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent();
                            setResult(Activity.RESULT_CANCELED, intent);
                            finish();
                        }
                    })
                    .setNegativeButton(getString(R.string.string_dialog_btn_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            count = 0;
                        }
                    }).create();
            dialog.show();
        }
    }

    @Override
    public void showSnackbarNoInternet(boolean isShow) {
        snackbarWrapper = new SnackbarWrapper(coordinatorLayout);
        snackbarWrapper.showLong(getString(R.string.string_snacbar_message_no_internet), null, null);
    }

    @Override
    public void showSnackbar(boolean isShow) {
        snackbarWrapper = new SnackbarWrapper(coordinatorLayout);
        snackbarWrapper.showIndefine(getString(R.string.string_snacbar_message_reset), getString(R.string.string_snacbar_action_reset), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(FragmentName.RESETPSWD_FRAGMENT_TAG);
            }
        });
    }

    @Override
    public void showSnackbarUserRegister() {
        snackbarWrapper = new SnackbarWrapper(coordinatorLayout);
        snackbarWrapper.showLong(getString(R.string.string_snacbar_message_user_registe), null, null);
    }

    @Override
    public void showSnackbarUserNotRegister() {
        snackbarWrapper = new SnackbarWrapper(coordinatorLayout);
        snackbarWrapper.showLong(getString(R.string.string_snacbar_message_user_not_registe), null, null);
    }

    @Override
    public void finishActivity() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void initComponents() {
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
    }
}
