package pl.com.bluesquare.android.login.login;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;
import pl.com.bluesquare.android.login.utils.FragmentName;
import pl.com.bluesquare.android.login.utils.events.ActionEvent;
import pl.com.bluesquare.android.login.utils.events.FragmentSwitchEvent;
import pl.com.bluesquare.android.login.utils.exceptions.EmailEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.UserNotRegisterException;
import pl.com.bluesquare.android.login.utils.validator.BaseValidator;
import pl.com.bluesquare.android.login.utils.validator.LoginValidator;
import pl.com.bluesquare.android.login.utils.validator.RegistrationValidator;
import pl.com.bluesquare.android.model.Auth;
import pl.com.bluesquare.android.model.User;
import pl.com.bluesquare.android.model.response.SigninResponse;
import pl.com.bluesquare.android.mvp.retrofit.presenter.RetrofitPresenter;
import pl.com.bluesquare.android.rest.clients.UserRestClient;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by work on 05.07.15.
 */
public class LoginPresenter extends RetrofitPresenter<LoginView, SigninResponse> {

    private BaseValidator validator;
    protected UserRestClient restClient;

    public LoginPresenter() {
        super();
        restClient = new UserRestClient();
        validator = new LoginValidator();
    }

    public void execute(String email, String paswd) {
        if (isViewAttached()) {
            if (validator.isValid(email, paswd)) {
                getView().showLoading();
                restClient.getSignin(getLoginData(email, paswd), this);

            } else {
                getView().showError(validator.getError());
            }
        }
    }

    @Override
    public void onResponse(Response<SigninResponse> response) {
        if (isViewAttached() && response.code() == 200) {
            getView().saveCreadential(response.body());
            getView().showSuccess();

        } else if (isViewAttached() && response.code() == 404) {
            getView().showError(new UserNotRegisterException());

        }
    }

    public void register() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new FragmentSwitchEvent(FragmentName.REGISTER_FRAGMENT_TAG));
        }
    }

    public void reset() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new ActionEvent(ActionEvent.ACTION_SHOW_SNACKBAR));
        }
    }

    public void noInternet() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new ActionEvent(ActionEvent.ACTION_SHOW_SNACKBAR_NOINTERNET));
        }
    }

    public void userNotRegister() {
        if (isViewAttached()) {
            EventBus.getDefault().post(new ActionEvent(ActionEvent.ACTION_SHOW_SNACKBAR_USER_NOT_REGISTER));
        }
    }

    private Map<String, String> getLoginData(final String email, final String paswd) {
        Map<String, String> map = new HashMap<>();
        map.put("username", email);
        map.put("password", paswd);
        return map;
    }


}
