package pl.com.bluesquare.android.login;

import android.app.Fragment;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import pl.com.bluesquare.android.mvp.core.fragment.MvpFragment;
import pl.com.bluesquare.android.mvp.core.presenter.MvpPresenter;
import pl.com.bluesquare.android.mvp.core.view.MvpView;

/**
 * Created by Luk.
 */
public abstract class AppBaseFragment <V extends MvpView, P extends MvpPresenter> extends MvpFragment<V, P> {



}
