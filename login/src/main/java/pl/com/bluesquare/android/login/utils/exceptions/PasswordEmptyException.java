package pl.com.bluesquare.android.login.utils.exceptions;

/**
 * Created by Luk.
 */
public class PasswordEmptyException extends Exception {

    public PasswordEmptyException() {
        super();
    }
}
