package pl.com.bluesquare.android.login.utils.exceptions;

/**
 * Created by Luk.
 */
public class EmailException extends Exception {


    public EmailException(String detailMessage) {
        super(detailMessage);
    }
}
