package pl.com.bluesquare.android.login.utils;

/**
 * Created by Luk.
 */
public class PreferenceName {


    /**
     * Store user id
     *
     * @return string
     */
    public static final String USER_ID_PREF = "user_id_pref";

    /**
     *  Store user login token
     *
     *  @return string
     */
    public static final String TOKEN_ID_PREF = "token_id_pref";


    /**
     * Store USER NAME
     */
    public static final String USER_NAME = "pref_user_name";


    /**
     * Store "remember me" on login screen
     *
     * @return boolean
     */
    public static final String LOGIN_REMEMBER_ME = "pref_remember_me";


    /**
     * Store login name
     *
     * @return string
     */
    public static final String LOGIN_EMAIL = "pref_login_email";

    /**
     * Store login password
     *
     * @return string
     */
    public static final String LOGIN_PASWD = "pref_login_paswd";


}
