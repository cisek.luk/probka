package pl.com.bluesquare.android.login.restorepassword;


import android.animation.LayoutTransition;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;


import java.net.UnknownHostException;

import pl.com.bluesquare.android.login.AppBaseFragment;
import pl.com.bluesquare.android.login.R;
import pl.com.bluesquare.android.login.utils.exceptions.EmailEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.login.utils.exceptions.UserNotRegisterException;
import pl.com.bluesquare.android.mvp.core.fragment.MvpFragment;

/**
 * Created by work on 28.06.15.
 */
public class PasswordRestoreFragment extends AppBaseFragment<PasswordRestoreView, PasswordRestorePresenter>
        implements PasswordRestoreView, View.OnClickListener {

    private TextInputLayout txtIEmail;
    private Button btnBack;
    private Button btnReset;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_reset_pswd;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents();
    }

    @Override
    protected PasswordRestorePresenter createPresenter() {
        return new PasswordRestorePresenter();
    }

    @Override
    public void showForm() {

    }

    @Override
    public void showLoading() {
        txtIEmail.setErrorEnabled(false);
    }

    @Override
    public void showError(Throwable t) {
        if (t instanceof EmptyFieldException) {
            txtIEmail.setError(getString(R.string.string_error_empty_email));

        } else if (t instanceof EmailFormatException) {
            txtIEmail.setError(getString(R.string.string_error_wrong_email_fromat));

        } else if (t instanceof UnknownHostException) {
            presenter.noInternet();

        } else if (t instanceof UserNotRegisterException) {
            presenter.userNotRegister();
        }

    }

    @Override
    public void showSuccess() {

    }

    private void initComponents() {
        txtIEmail = (TextInputLayout) getView().findViewById(R.id.reset_edtxt_email);
        btnBack = (Button) getView().findViewById(R.id.reset_btn_back);
        btnReset = (Button) getView().findViewById(R.id.reset_btn_reset);

        btnBack.setOnClickListener(this);
        btnReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.reset_btn_back) {
            presenter.back();

        } else if (v.getId() == R.id.reset_btn_reset) {
            presenter.execute(txtIEmail.getEditText().getText().toString());
        }
    }
}
