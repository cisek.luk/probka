package pl.com.bluesquare.android.login.userdetails;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import java.net.UnknownHostException;

import pl.com.bluesquare.android.login.AppBaseFragment;
import pl.com.bluesquare.android.login.R;
import pl.com.bluesquare.android.login.utils.BundleNames;
import pl.com.bluesquare.android.login.utils.PreferenceName;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.utils.PreferenceHelper;

/**
 * Created by Luk on 07.10.15.
 */
public class UserDetailFragment extends AppBaseFragment<UserDetailsView, UserDetailsPresenter>
        implements UserDetailsView, View.OnClickListener {

    private TextInputLayout txtINick;
    private TextInputLayout txtIFirstName;
    private TextInputLayout txtILastName;
    private Button btnSkip;
    private Button btnAdd;

    private PreferenceHelper mPreferenceHelper;

    @Override
    protected UserDetailsPresenter createPresenter() {
        return new UserDetailsPresenter();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_user_details;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onRestore(savedInstanceState);
        mPreferenceHelper = new PreferenceHelper(getActivity());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BundleNames.USER_DETAILS_BUNDLE_NICK, txtINick.getEditText().getText().toString());
        outState.putString(BundleNames.USER_DETAILS_BUNDLE_FIRST_NAME, txtIFirstName.getEditText().getText().toString());
        outState.putString(BundleNames.USER_DETAILS_BUNDLE_LAST_NAME, txtILastName.getEditText().getText().toString());
    }

    @Override
    public void showForm() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void showError(Throwable t) {
        if (t instanceof EmptyFieldException && t.getMessage().equals("0")) {
            txtINick.setError(getString(R.string.string_error_empty_email));

        } else if (t instanceof EmptyFieldException && t.getMessage().equals("1")) {
            txtIFirstName.setError(getString(R.string.string_error_empty_password));

        } else if (t instanceof EmptyFieldException && t.getMessage().equals("2")) {
            txtILastName.setError(getString(R.string.string_error_empty_confirmpassword));

        } else if (t instanceof UnknownHostException) {
            presenter.nointernet();
        }
    }

    @Override
    public void showSuccess() {
        Intent intent = new Intent();
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ud_btn_skip) {
            presenter.skip();

        } else if (v.getId() == R.id.up_btn_add) {
            presenter.execute(
                    mPreferenceHelper.getString(PreferenceName.USER_ID_PREF),
                    txtINick.getEditText().toString(),
                    txtIFirstName.getEditText().toString(),
                    txtILastName.getEditText().toString());
        }
    }

    private void initComponents() {
        txtINick = (TextInputLayout) getView().findViewById(R.id.ud_edtxt_nick_name);
        txtIFirstName = (TextInputLayout) getView().findViewById(R.id.ud_edtxt_first_name);
        txtILastName = (TextInputLayout) getView().findViewById(R.id.ud_edtxt_last_name);
        btnSkip = (Button) getView().findViewById(R.id.ud_btn_skip);
        btnAdd = (Button) getView().findViewById(R.id.up_btn_add);
        btnSkip.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
    }

    private void onRestore(Bundle bundle) {
        if (bundle != null) {
            txtINick.getEditText().setText(bundle.getString(BundleNames.USER_DETAILS_BUNDLE_NICK));
            txtIFirstName.getEditText().setText(bundle.getString(BundleNames.USER_DETAILS_BUNDLE_FIRST_NAME));
            txtILastName.getEditText().setText(bundle.getString(BundleNames.USER_DETAILS_BUNDLE_LAST_NAME));

        }
    }


}
