package pl.com.bluesquare.android.login.utils.validator;

import android.text.TextUtils;

import java.util.Map;
import java.util.regex.Pattern;

import pl.com.bluesquare.android.login.utils.exceptions.EmailEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.EmailException;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordNotMatchException;

/**
 * Created by Luk.
 */
public class RegistrationValidator extends BaseValidator {

    @Override
    public boolean isValid(String... values) {
        if (TextUtils.isEmpty(values[0])) {
            error = new EmptyFieldException("0");
            return false;

        } else if (TextUtils.isEmpty(values[1])) {
            error = new EmptyFieldException("1");
            return false;

        } else if (TextUtils.isEmpty(values[2])) {
            error = new EmptyFieldException("2");
            return false;

        }

        if (!isEmailValid(values[0])) {
            return false;

        } else if (!isPasswordValid(values[1], values[2])) {
            return false;

        } else if (values.length != 3) {
            error = new IllegalArgumentException("To many parameters!");
            return false;

        } else {
            return true;
        }
    }

}
