package pl.com.bluesquare.android.login.utils.validator;

import android.text.TextUtils;
import android.util.Log;

import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;

/**
 * Created by Luk on 08.10.15.
 */
public class ResetPasswordValidator extends BaseValidator {

    @Override
    public boolean isValid(String... values) {
        if (TextUtils.isEmpty(values[0])) {
            error = new EmptyFieldException("0");
            return false;

        } else if (!isEmailValid(values[0])) {
            return false;

        } else if (values.length != 1) {
            error = new IllegalArgumentException("To many parameters!");
            return false;

        } else {
            return true;
        }
    }
}
