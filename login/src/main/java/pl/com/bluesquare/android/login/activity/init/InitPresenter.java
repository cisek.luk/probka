package pl.com.bluesquare.android.login.activity.init;

import android.app.Fragment;
import android.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import pl.com.bluesquare.android.login.login.LoginFragment;
import pl.com.bluesquare.android.login.registration.RegistrationFragment;
import pl.com.bluesquare.android.login.restorepassword.PasswordRestoreFragment;
import pl.com.bluesquare.android.login.userdetails.UserDetailFragment;
import pl.com.bluesquare.android.login.utils.FragmentName;
import pl.com.bluesquare.android.login.utils.events.ActionEvent;
import pl.com.bluesquare.android.login.utils.events.FragmentSwitchEvent;
import pl.com.bluesquare.android.mvp.core.presenter.BasePresenter;

/**
 * Created by work on 05.07.15.
 */
public class InitPresenter extends BasePresenter<InitView> {


    public Fragment getFragment(FragmentManager fragmentManager, String fragmentName) {
        if (TextUtils.isEmpty(fragmentName)) {
            throw new IllegalArgumentException("Fragment name is empty or null!");
        }

        if (fragmentManager.findFragmentByTag(fragmentName) != null) {
            Log.d(TAG, "fragment found");
            return fragmentManager.findFragmentByTag(fragmentName);
        }

        if (FragmentName.LOGIN_FRAGMENT_TAG.equals(fragmentName)) {
            Log.d(TAG, "return fragment login");
            return new LoginFragment();
        }

        if (FragmentName.REGISTER_FRAGMENT_TAG.equals(fragmentName)) {
            Log.d(TAG, "return fragment register");
            return new RegistrationFragment();
        }

        if (FragmentName.USERDETAILS_FRAGMENT_TAG.equals(fragmentName)) {
            Log.d(TAG, "return fragment register");
            return new UserDetailFragment();
        }

        if (FragmentName.RESETPSWD_FRAGMENT_TAG.equals(fragmentName)) {
            Log.d(TAG, "return fragment register");
            return new PasswordRestoreFragment();
        }

        return null;
    }

    @Override
    public void attachView(InitView view) {
        super.attachView(view);
        EventBus.getDefault().register(this);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(FragmentSwitchEvent ev) {
        if (isViewAttached() && !ev.fragmentName.equals(FragmentName.FINISH)) {
            getView().switchFragment(ev.fragmentName);

        } else if (isViewAttached()) {
            getView().finishActivity();
        }
    }

    @Subscribe
    public void onEvent(ActionEvent ev) {
        if (isViewAttached()) {
            switch (ev.actionId) {
                case ActionEvent.ACTION_SHOW_SNACKBAR:
                    getView().showSnackbar(true);

                    break;
                case ActionEvent.ACTION_SHOW_SNACKBAR_NOINTERNET:
                    getView().showSnackbarNoInternet(true);
                    break;

                case ActionEvent.ACTION_SHOW_SNACKBAR_USER_REGISTER:
                    getView().showSnackbarUserRegister();
                    break;

                case ActionEvent.ACTION_SHOW_SNACKBAR_USER_NOT_REGISTER:
                    getView().showSnackbarUserNotRegister();
                    break;

                default:
                    throw new IllegalArgumentException("Unknown action id!");
            }
        }
    }


}
