package pl.com.bluesquare.android.login.registration;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.net.UnknownHostException;

import pl.com.bluesquare.android.login.AppBaseFragment;
import pl.com.bluesquare.android.login.R;
import pl.com.bluesquare.android.login.utils.BundleNames;
import pl.com.bluesquare.android.login.utils.PreferenceName;
import pl.com.bluesquare.android.login.utils.exceptions.EmailEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.EmailFormatException;
import pl.com.bluesquare.android.login.utils.exceptions.EmptyFieldException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordEmptyException;
import pl.com.bluesquare.android.login.utils.exceptions.PasswordNotMatchException;
import pl.com.bluesquare.android.login.utils.exceptions.UserRegisterException;
import pl.com.bluesquare.android.model.response.SigninResponse;
import pl.com.bluesquare.android.model.response.SignupResponse;
import pl.com.bluesquare.android.mvp.retrofit.exception.NetworkException;
import pl.com.bluesquare.android.mvp.retrofit.exception.UnexpectedStatusCodeException;
import pl.com.bluesquare.android.utils.PreferenceHelper;

/**
 * Created by Luk on 28.06.15.
 */
public class RegistrationFragment extends AppBaseFragment<RegistrationView, RegistrationPresenter>
        implements RegistrationView, View.OnClickListener {

    TextInputLayout txtILogin;
    TextInputLayout txtIPassword;
    TextInputLayout txtIConfPassword;
    Button btnBack;
    Button btnRegister;

    private String email;
    private String password;
    private String confirmPassword;

    private PreferenceHelper mPreferenceHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mPreferenceHelper = new PreferenceHelper(getActivity());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_registration;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onRestore(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BundleNames.REGISTRATION_BUNDLE_EMAIL, txtILogin.getEditText().getText().toString());
        outState.putString(BundleNames.REGISTRATION_BUNDLE_PASSWORD, txtIPassword.getEditText().getText().toString());
        outState.putString(BundleNames.REGISTRATION_BUNDLE_CONFIRM_PASSWROD, txtIConfPassword.getEditText().getText().toString());
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }

    @Override
    protected RegistrationPresenter createPresenter() {
        return new RegistrationPresenter();
    }

    @Override
    public void showForm() {
    }

    @Override
    public void showLoading() {
        reset();
    }

    @Override
    public void showError(Throwable t) {
        if (t instanceof EmptyFieldException && t.getMessage().equals("0")) {
            txtILogin.setError(getString(R.string.string_error_empty_email));

        } else if (t instanceof EmptyFieldException && t.getMessage().equals("1")) {
            txtIPassword.setError(getString(R.string.string_error_empty_password));

        } else if (t instanceof EmptyFieldException && t.getMessage().equals("2")) {
            txtIConfPassword.setError(getString(R.string.string_error_empty_confirmpassword));

        } else if (t instanceof EmailFormatException) {
            txtILogin.setError(getString(R.string.string_error_wrong_email_fromat));

        } else if (t instanceof PasswordNotMatchException) {
            txtIPassword.setError(getString(R.string.string_error_empty_not_match));
            txtIConfPassword.setError(getString(R.string.string_error_empty_not_match));

        } else if (t instanceof UserRegisterException) {
            presenter.userRegister();

        } else if (t instanceof UnknownHostException) {
            presenter.noInternet();

        }

        Log.d("TAG", "error2 :: " + t.getMessage());
    }

    @Override
    public void showSuccess() {
        presenter.userDetails();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.register_btn_login) {
            reset();
            presenter.login();

        } else if (v.getId() == R.id.register_btn_register) {
            getRegistrationData();
            presenter.execute(email, password, confirmPassword);
        }
    }

    @Override
    public void saveCreadential(SignupResponse user) {
        mPreferenceHelper.setString(PreferenceName.USER_ID_PREF, user.objectId);
        mPreferenceHelper.setString(PreferenceName.TOKEN_ID_PREF, user.sessionToken);
//        mPreferenceHelper.setString(PreferenceName.USER_NAME, user.username);
    }

    private void initComponents() {
        txtILogin = (TextInputLayout) getView().findViewById(R.id.register_edtxt_email);
        txtIPassword = (TextInputLayout) getView().findViewById(R.id.register_edtxt_pswd);
        txtIConfPassword = (TextInputLayout) getView().findViewById(R.id.register_edtxt_confirm_pswd);
        btnBack = (Button) getView().findViewById(R.id.register_btn_login);
        btnRegister = (Button) getView().findViewById(R.id.register_btn_register);
        btnBack.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
    }

    private void getRegistrationData() {
        email = txtILogin.getEditText().getText().toString();
        password = txtIPassword.getEditText().getText().toString();
        confirmPassword = txtIConfPassword.getEditText().getText().toString();
    }

    private void onRestore(Bundle bundle) {
        if (bundle != null) {
            txtILogin.getEditText().setText(bundle.getString(BundleNames.REGISTRATION_BUNDLE_EMAIL));
            txtIPassword.getEditText().setText(bundle.getString(BundleNames.REGISTRATION_BUNDLE_PASSWORD));
            txtIConfPassword.getEditText().setText(bundle.getString(BundleNames.REGISTRATION_BUNDLE_CONFIRM_PASSWROD));
        }
    }

    private void reset() {
        txtILogin.setErrorEnabled(false);
        txtIPassword.setErrorEnabled(false);
        txtIConfPassword.setErrorEnabled(false);
    }

}
