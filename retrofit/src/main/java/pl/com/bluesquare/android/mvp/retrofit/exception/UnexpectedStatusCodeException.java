package pl.com.bluesquare.android.mvp.retrofit.exception;

/**
 * Created by work on 05.07.15.
 */
public class UnexpectedStatusCodeException extends Exception {

    private int statusCode;

    public UnexpectedStatusCodeException(int statusCode) {
        super("An unexcpected http status code in http response: " + statusCode);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
