package pl.com.bluesquare.android.mvp.retrofit.exception;

/**
 * Created by work on 05.07.15.
 */
public class NetworkException extends Exception {

    public NetworkException() {
    }

    public NetworkException(String detailMessage) {
        super(detailMessage);
    }

    public NetworkException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public NetworkException(Throwable throwable) {
        super(throwable);
    }
}
