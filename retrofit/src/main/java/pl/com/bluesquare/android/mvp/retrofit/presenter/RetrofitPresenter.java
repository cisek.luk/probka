package pl.com.bluesquare.android.mvp.retrofit.presenter;

import android.util.Log;

import pl.com.bluesquare.android.mvp.core.presenter.BasePresenter;
import pl.com.bluesquare.android.mvp.retrofit.view.MvpRetrofitView;
import retrofit.Callback;

/**
 * Created by work on 05.07.15.
 */
public abstract class RetrofitPresenter<V extends MvpRetrofitView, M> extends BasePresenter<V>
        implements Callback<M> {


    @Override
    public void onFailure(Throwable error) {
        if(isViewAttached()) {
            Log.d("TAG", "error retrofit :: " + error.getClass().getName());
            getView().showError(error);
        }
    }


}
